TEMPLATE = app
TARGET = restream
QT = core network
MOC_DIR = ../.build
OBJECTS_DIR = ../.build
DESTDIR = ..
INCLUDEPATH += ../libqxt/include ../libqxt/include/QxtCore ../libqxt/include/QxtNetwork ../libqxt/include/QxtWeb
INCLUDEPATH += ../libqxt/src ../libqxt/src/core ../libqxt/src/network ../libqxt/src/web
LIBS += ../libqxt/lib/libQxtWeb.a ../libqxt/lib/libQxtCore.a ../libqxt/lib/libQxtNetwork.a
LIBS -= -lQxtWeb -lQxtNetwork -lQxtCore
DEFINES -= HAVE_OPENSSL
DEFINES += QT_NO_OPENSSL
CONFIG += debug

SOURCES += main.cpp
