#include <QCoreApplication>
#include "QxtHttpServerConnector"
#include "QxtHttpSessionManager"
#include "QxtWebSlotService"
#include "QxtWebPageEvent"
#include <QProcess>
#include <QtDebug>

class StreamService : public QxtWebSlotService
{
Q_OBJECT
public:
  StreamService(const QString& source, QxtAbstractWebSessionManager* sm, QObject* parent = 0) : QxtWebSlotService(sm, parent), source(source) {}

public slots:
  void index(QxtWebRequestEvent* event)
  {
    qDebug() << "Serving...";
    QProcess* process = new QProcess(this);
    QxtWebPageEvent* response = new QxtWebPageEvent(event->sessionID, event->requestID, process);
    process->start(QString("ffmpeg -f pulse -i %1 -f mp3 -").arg(source));
    response->contentType = "audio/mpeg";
    postEvent(response);
  }

private:
  QString source;
};

int main(int argc, char** argv)
{
  if (argc < 2) {
    qDebug() << "Usage:" << argv[0] << " source";
    return 1;
  }
  QCoreApplication app(argc, argv);

  QxtHttpServerConnector connector;
  QxtHttpSessionManager session;
  session.setPort(42069);
  session.setConnector(&connector);

  StreamService svc(app.arguments()[1], &session);
  session.setStaticContentService(&svc);

  session.start();
  return app.exec();
}

#include "main.moc"
