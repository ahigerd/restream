restream: libqxt/lib/libQxtCore.a libqxt/lib/libQxtNetwork.a libqxt/lib/libQxtWeb.a src/Makefile $(wildcard src/*.h) $(wildcard src/*.cpp)
	$(MAKE) -C src

qmake: src/restream.pro
	cd src && qmake

src/Makefile: src/restream.pro
	cd src && qmake

libqxt:
	git clone https://bitbucket.org/libqxt/libqxt

libqxt/Makefile: libqxt
	cd libqxt && ./configure -nomake docs -nomake widgets -nomake sql -no-openssl -static -no-db -no-xrandr -no-zeroconf -debug

libqxt/lib/libQxtCore.a libqxt/lib/libQxtNetwork.a libqxt/lib/libQxtWeb.a: libqxt/Makefile
	$(MAKE) -C libqxt
	rm libqxt/lib/*.prl

clean:
	-$(MAKE) -C src clean

distclean:
	-$(MAKE) -C src distclean
	-$(MAKE) -C libqxt distclean
